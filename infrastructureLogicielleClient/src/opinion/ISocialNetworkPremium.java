package opinion;

import exceptions.BadEntryException;
import exceptions.NotItemException;
import exceptions.NotMemberException;

public interface ISocialNetworkPremium extends ISocialNetwork {
    public void reviewOpinion(String login, String password, String title, String reviewerLogin, float mark) throws BadEntryException, NotMemberException, NotItemException;
}
