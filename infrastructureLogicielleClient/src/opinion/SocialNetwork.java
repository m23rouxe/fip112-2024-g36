package opinion;

import java.util.LinkedList;

import elements.*;
import exceptions.BadEntryException;
import exceptions.ItemBookAlreadyExistsException;
import exceptions.ItemFilmAlreadyExistsException;
import exceptions.MemberAlreadyExistsException;
import exceptions.NotItemException;
import exceptions.NotMemberException;

/**
 * Represents a social network.
 */
public class SocialNetwork implements ISocialNetwork {

	/** The list of members in the social network. */
	LinkedList<Member> members;

	/** The list of items in the social network. */
	LinkedList<Item> items;

	/**
	 * Constructs a new social network.
	 */
	public SocialNetwork() {
		members = new LinkedList<>();
		items = new LinkedList<>();
	}

	/**
	 * Gets the list of members in the social network.
	 *
	 * @return the list of members in the social network
	 */
	public LinkedList<Member> getMembers() {
		return members;
	}

	/**
	 * Gets the list of items in the social network.
	 *
	 * @param itemType the type of item to retrieve (e.g., ItemBook.class, ItemFilm.class)
	 * @param <T> the type of item
	 * @return the list of items of the specified type in the social network
	 */
	public <T> LinkedList<T> getItems(Class<T> itemType) {
		LinkedList<T> output = new LinkedList<>();
		for (Item item : items) {
			if (itemType.isInstance(item)) {
				output.add(itemType.cast(item));
			}
		}
		return output;
	}

	/**
	 * Returns a string representation of this social network.
	 *
	 * @return a string representation of this social network
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("\nmembers : \n\n");
		for (Member m : getMembers()) {
			builder.append(m.toString()).append("\n");
		}
		builder.append("\nlivres : \n\n");
		for (ItemBook b : getItems(ItemBook.class)) {
			builder.append(b.toString()).append("\n");
		}
		builder.append("\nfilms : \n\n");
		for(ItemFilm f : getItems(ItemFilm.class)){
			builder.append(f.toString()).append("\n");
		}

		return builder.toString();
	}

	/**
	 * Returns the number of members in the social network.
	 *
	 * @return the number of members in the social network
	 */
	@Override
	public int nbMembers() {
		return members.size();
	}

	/**
	 * Returns the number of films in the social network.
	 *
	 * @return the number of films in the social network
	 */
	@Override
	public int nbFilms() {
		return getItems(ItemFilm.class).size();
	}

	/**
	 * Returns the number of books in the social network.
	 *
	 * @return the number of books in the social network
	 */
	@Override
	public int nbBooks() {
		return getItems(ItemBook.class).size();
	}

	/**
	 * Gets a member by their login.
	 *
	 * @param login the login of the member
	 * @return the member with the specified login, or null if not found
	 */
	public Member getMember(String login) {
		for (Member member : members) {
			if (member.equals(login)) {
				return member;
			}
		}
		return null;
	}

	/**
	 * Gets an item by its title and type.
	 *
	 * @param title the title of the item
	 * @param type the type of the item (e.g., ItemBook.class, ItemFilm.class)
	 * @param <T> the type of the item
	 * @return the item with the specified title and type, or null if not found
	 */
	public <T> Item getItem(String title, Class<T> type) {
		for (Item item : items) {
			if (item.equals(title, type)) {
				return item;
			}
		}
		return null;
	}

	/**
	 * Adds a new member to the social network.
	 *
	 * @param login the login of the member
	 * @param password the password of the member
	 * @param profile the profile of the member
	 * @throws BadEntryException if the provided parameters are invalid
	 * @throws MemberAlreadyExistsException if a member with the same login already exists
	 */
	@Override
	public void addMember(String login, String password, String profile)
			throws BadEntryException, MemberAlreadyExistsException {
		for (Member m : members) {
			if (m.equals(login)) {
				throw new MemberAlreadyExistsException();
			}
		}
		members.add(new Member(login, password, profile));
	}

	/**
	 * Adds a new film to the social network.
	 *
	 * @param login the login of the member
	 * @param password the password of the member
	 * @param title the title of the film
	 * @param kind the kind or genre of the film
	 * @param director the director of the film
	 * @param scriptwriter the scriptwriter of the film
	 * @param duration the duration of the film in minutes
	 * @throws BadEntryException if the provided parameters are invalid
	 * @throws NotMemberException if the member doesn't exist
	 * @throws ItemFilmAlreadyExistsException if a film with the same title already exists
	 */
	@Override
	public void addItemFilm(String login, String password, String title,
							String kind, String director, String scriptwriter, int duration)
			throws BadEntryException, NotMemberException, ItemFilmAlreadyExistsException {
		Member m = getMember(login);
		if (m != null && m.isPassword(password)) {
			if (getItem(title, ItemFilm.class) != null) {
				throw new ItemFilmAlreadyExistsException();
			}
			items.add(new ItemFilm(title, kind, director, scriptwriter, duration));
		} else {
			throw new NotMemberException("Login or password incorrect");
		}
	}

	/**
	 * Adds a new book to the social network.
	 *
	 * @param login the login of the member
	 * @param password the password of the member
	 * @param title the title of the book
	 * @param kind the kind or genre of the book
	 * @param author the author of the book
	 * @param nbPages the number of pages in the book
	 * @throws BadEntryException if the provided parameters are invalid
	 * @throws NotMemberException if the member doesn't exist
	 * @throws ItemBookAlreadyExistsException if a book with the same title and author already exists
	 */
	@Override
	public void addItemBook(String login, String password, String title,
							String kind, String author, int nbPages) throws BadEntryException,
			NotMemberException, ItemBookAlreadyExistsException {
		Member m = getMember(login);
		if (m != null && m.isPassword(password)) {
			if (getItem(title, ItemBook.class) != null) {
				throw new ItemBookAlreadyExistsException();
			}
			items.add(new ItemBook(title, kind, author, nbPages));
		} else {
			throw new NotMemberException("Login or password incorrect");
		}
	}

	/**
	 * Reviews a film.
	 *
	 * @param login the login of the member
	 * @param password the password of the member
	 * @param title the title of the film
	 * @param mark the mark given to the film
	 * @param comment the comment on the film
	 * @return the mean reviews of the film
	 * @throws BadEntryException if the provided parameters are invalid
	 * @throws NotMemberException if the member doesn't exist
	 * @throws NotItemException if the film doesn't exist
	 */
	@Override
	public float reviewItemFilm(String login, String password, String title,
								float mark, String comment) throws BadEntryException,
			NotMemberException, NotItemException {
		Member m = getMember(login);
		ItemFilm b = (ItemFilm) getItem(title, ItemFilm.class);
		if (m == null || !m.isPassword(password)) {
			throw new NotMemberException("Nom d'utilisateur ou mot de passe incorrect");
		}
		if (b == null) {
			throw new NotItemException("Film non enregistré");
		}
		Review r = new Review(mark, comment);
		b.addReview(m, r);
		m.addReview(r);
		return b.getMeanReviews();
	}

	/**
	 * Reviews a book.
	 *
	 * @param login the login of the member
	 * @param password the password of the member
	 * @param title the title of the book
	 * @param mark the mark given to the book
	 * @param comment the comment on the book
	 * @return the mean reviews of the book
	 * @throws BadEntryException if the provided parameters are invalid
	 * @throws NotMemberException if the member doesn't exist
	 * @throws NotItemException if the book doesn't exist
	 */
	@Override
	public float reviewItemBook(String login, String password, String title,
								float mark, String comment) throws BadEntryException,
			NotMemberException, NotItemException {
		Member m = getMember(login);
		ItemBook b = (ItemBook) getItem(title, ItemBook.class);
		if (m == null || !m.isPassword(password)) {
			throw new NotMemberException("Nom d'utilisateur ou mot de passe incorrect");
		}
		if (b == null) {
			throw new NotItemException("Livre non enregistré");
		}
		Review r = new Review(mark, comment);
		b.addReview(m, r);
		m.addReview(r);
		return b.getMeanReviews();
	}

	/**
	 * Consults items.
	 *
	 * @param title the title of the item
	 * @return a list of string representations of items with the specified title
	 * @throws BadEntryException if the provided title is invalid
	 */
	@Override
	public LinkedList<String> consultItems(String title)
			throws BadEntryException {
		LinkedList<String> list = new LinkedList<>();
		if (title == null || title.trim().isEmpty()) {
			throw new BadEntryException("Format du titre recherché incorrect");
		}
		for (Item item : items) {
			if (item.equals(title, Item.class)) {
				list.add(item.toString());
			}
		}
		return list;
	}
}