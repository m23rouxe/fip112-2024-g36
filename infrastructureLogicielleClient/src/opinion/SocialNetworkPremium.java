package opinion;

import elements.Item;
import elements.ItemBook;
import elements.ItemFilm;
import elements.Member;
import exceptions.*;

import java.util.LinkedList;
import java.util.Scanner;

public class SocialNetworkPremium extends SocialNetwork implements ISocialNetworkPremium {
    public SocialNetworkPremium() {
        super();
    }

    public LinkedList<Item> getItemsByTitle(String title) throws NotItemException {
        LinkedList<Item> items = new LinkedList<>();
        for (Item item : super.items) {
            if (item.getTitle().equals(title)) {
                items.add(item);
            }
        }
        return items;
    }

    public void reviewOpinion(String login, String password, String title, String reviewerLogin, float mark) throws NotMemberException, NotItemException, BadEntryException {
        Member member = getMember(login);
        if(member == null || !member.isPassword(password)) throw new NotMemberException("Le membre qui veut noter un avis n'est pas enregistré");
        if(mark < 0 || mark > 5) throw new BadEntryException("format de note incorrect");

        LinkedList<Item> listeItems = getItemsByTitle(title);
        if(listeItems.isEmpty()) throw new NotItemException("il n'existe pas d'article associé au titre : "+title);

        Item selectedItem = listeItems.getFirst();
        if(listeItems.size() > 1) {
            System.out.println("il existe plusieurs articles associés au titre : "+title);
            for(Item item : listeItems) {
                System.out.println(item);
            }

            Scanner scanner = new Scanner(System.in);
            int userInput;
            do {
                System.out.println("entrer l'indice de l'article à sélectionner (0 - "+(listeItems.size()-1)+"): ");
                userInput = scanner.nextInt();
                if(userInput < 0 || userInput > listeItems.size()-1) {
                    System.out.println("indice incorrect");
                }
            } while (listeItems.get(userInput) == null);
            selectedItem = listeItems.get(userInput);
        }

        Member reviewer = getMember(reviewerLogin);
        if(reviewer == null) throw new NotMemberException("Le membre spécifié n'a pas ajouté d'avis sur cet article");

        selectedItem.addOpinionToReview(login,reviewer, mark);
        System.out.printf("added opinion from %s on %s's review\n", login, reviewerLogin);
    }
}

