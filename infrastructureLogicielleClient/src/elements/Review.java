package elements;

import exceptions.BadEntryException;

import java.util.HashMap;

/**
 * Represents a review in the social network.
 */
public class Review {

    /** The mark given in the review, ranging from 0.0 to 5.0. */
    float mark;

    /** The comment provided in the review. */
    String comment;

    HashMap<String, Float> opinions;

    /**
     * Constructs a new Review with the specified mark and comment.
     *
     * @param mark the mark given in the review, should be between 0.0 and 5.0
     * @param comment the comment provided in the review
     * @throws BadEntryException if the mark is out of range or the comment is invalid
     */
    public Review(float mark, String comment) throws BadEntryException {
        if (mark < 0.0 || mark > 5.0) {
            throw new BadEntryException("note invalide (doit être comprise entre 0 et 5)");
        }
        if (comment == null || comment.trim().isEmpty()) {
            throw new BadEntryException("format du commentaire invalide");
        }
        // If all parameters are valid, assign them to the corresponding instance variables.
        this.mark = mark;
        this.comment = comment;
        this.opinions = new HashMap<>();
    }

    /**
     * Gets the mark given in the review.
     *
     * @return the mark given in the review
     */
    public float getMark() {
        return mark;
    }

    public float getMeanOpinions(){
        if(opinions.isEmpty()) return 2.5f;
        float sum = 0f;
        for (float opinion : opinions.values()) {
            sum += opinion;
        }
        return sum / opinions.size();
    }

    /**
     * Gets the comment provided in the review.
     *
     * @return the comment provided in the review
     */
    public String getComment() {
        return comment;
    }

    public void addOpinion(String login, float mark){
        opinions.put(login, mark);
    }
}
