package elements;

import exceptions.BadEntryException;

/**
 * Represents a film item in the social network. Inherits from the Item class.
 */
public class ItemFilm extends Item {

    /** The director of the film. */
    String director;

    /** The scenarist of the film. */
    String scenarist;

    /** The duration of the film in minutes. */
    int duration;

    /**
     * Constructs a new ItemFilm with the specified title, kind, director, scenarist, and duration.
     *
     * @param title the title of the film
     * @param kind the kind or genre of the film
     * @param director the director of the film
     * @param scenarist the scenarist of the film
     * @param duration the duration of the film in minutes
     * @throws BadEntryException if any of the parameters are invalid
     */
    public ItemFilm(String title, String kind, String director, String scenarist, int duration) throws BadEntryException {
        super(title, kind);
        if (director == null || director.trim().isEmpty()) {
            throw new BadEntryException("format du réalisateur incorrect");
        }
        if (scenarist == null || scenarist.trim().isEmpty()) {
            throw new BadEntryException("format du scénariste incorrect");
        }
        if (duration <= 0) {
            throw new BadEntryException("valeur de durée incorrecte");
        }

        // If all parameters are valid, assign them to the corresponding instance variables.
        this.director = director;
        this.scenarist = scenarist;
        this.duration = duration;
    }

    /**
     * Gets the director of the film.
     *
     * @return the director of the film
     */
    public String getDirector() {
        return director;
    }

    /**
     * Gets the scenarist of the film.
     *
     * @return the scenarist of the film
     */
    public String getScenarist() {
        return scenarist;
    }

    /**
     * Gets the duration of the film.
     *
     * @return the duration of the film in minutes
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Returns a string representation of the film item.
     *
     * @return a formatted string representation of the film item, including title, director, scenarist, kind, and duration
     */
    @Override
    public String toString() {
        // Return a formatted string representation of this film item,
        // including its title, director, scenarist, kind, and duration.
        return String.format("titre: %s\nréalisateur: %s\nscénariste: %s\ngenre: %s\ndurée: %d minutes\nnote moyenne : %s\n", getTitle(), getDirector(), getScenarist(), getKind(), getDuration(), Float.isNaN(getMeanReviews()) ? "pas encore de reviews" : getMeanReviews());
    }
}
