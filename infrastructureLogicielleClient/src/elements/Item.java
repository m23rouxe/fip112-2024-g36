package elements;

import exceptions.BadEntryException;

import java.util.HashMap;

/**
 * Represents an item in the social network.
 */
public class Item {

    /** The title of the item. */
    protected String title;

    /** The kind or genre of the item. */
    protected String kind;

    /** The reviews associated with the item. */
    protected HashMap<Member, Review> reviews;

    /**
     * Constructs a new item with the specified title and kind.
     *
     * @param title the title of the item
     * @param kind the kind or genre of the item
     * @throws BadEntryException if the provided parameters are invalid
     */
    public Item(String title, String kind) throws BadEntryException {
        if (title == null || title.trim().isEmpty()) {
            throw new BadEntryException("Format du titre incorrect");
        }
        if (kind == null || kind.trim().isEmpty()) {
            throw new BadEntryException("Format du genre incorrect");
        }
        // Initialize the reviews HashMap
        this.title = title;
        this.kind = kind;
        reviews = new HashMap<>();
    }

    /**
     * Adds a review to the item.
     *
     * @param publisher the member who published the review
     * @param review the review to add
     */
    public void addReview(Member publisher, Review review) {
        reviews.put(publisher, review);
    }

    /**
     * Gets the number of reviews for the item.
     *
     * @return the number of reviews for the item
     */
    public int nbReviews() {
        return reviews.size();
    }

    /**
     * Checks if the item's title matches the specified title and type.
     *
     * @param title the title to compare
     * @param type the type of the item (e.g., ItemBook.class, ItemFilm.class)
     * @param <T> the type of the item
     * @return true if the titles match and the item is of the specified type, false otherwise
     */
    public <T> boolean equals(String title, Class<T> type) {
        if (title == null) {
            return false;
        }
        return (getTitle().replaceAll("\\s+", "").equalsIgnoreCase(title.replaceAll("\\s+", "")) && type.isInstance(this));
    }

    /**
     * Calculates the mean reviews of the item.
     *
     * @return the mean reviews of the item
     */
    public Float getMeanReviews() {
        float sum = 0;
        float karmaSum = 0;
        for (Member m : reviews.keySet()) {
            sum += reviews.get(m).getMark()*m.getKarma();
            karmaSum += m.getKarma();
        }
        if(karmaSum == 0f) return Float.NaN;
        return Math.round(sum / karmaSum * 100) / 100f;
    }

    /**
     * Gets the title of the item.
     *
     * @return the title of the item
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the kind or genre of the item.
     *
     * @return the kind or genre of the item
     */
    public String getKind() {
        return kind;
    }

    /**
     * Gets the reviews associated with the item.
     *
     * @return the reviews associated with the item
     */
    public HashMap<Member, Review> getReviews() {
        return reviews;
    }

    public void addOpinionToReview(String login, Member reviewer, float opinion){
        getReviews().get(reviewer).addOpinion(login, opinion);
        reviewer.updateKarma();
    }
}
