package elements;

import exceptions.BadEntryException;

import java.util.LinkedList;

/**
 * Represents a member of the Social Network.
 */
public class Member {

    /** The login of the member. */
    String login;

    /** The password of the member. */
    String password;

    /** The profile description of the member. */
    String profile;

    LinkedList<Review> reviews;

    Float karma;

    /**
     * Constructs a new Member with the specified login, password, and profile.
     *
     * @param login the login of the member
     * @param password the password of the member
     * @param profile the profile description of the member
     * @throws BadEntryException if any of the parameters are invalid
     */
    public Member(String login, String password, String profile) throws BadEntryException {
        // Check if any of the parameters are null, empty, or if the password is too short.
        if (login == null || login.trim().isEmpty() || password == null || password.trim().length() < 4 ||
                profile == null) {
            // Throw a BadEntryException with an appropriate message if any parameter is invalid
            throw new BadEntryException("Bad entry");
        }
        // If all parameters are valid, assign them to the corresponding instance variables.
        this.login = login;
        this.password = password;
        this.profile = profile;
        this.reviews = new LinkedList<>();
        this.karma = 1f;
    }

    /**
     * Checks if the provided login matches the member's login.
     *
     * @param login the login to check
     * @return true if the provided login matches the member's login, false otherwise
     */
    public boolean equals(String login) {
        // Check if the provided login is not null and matches the member's login, ignoring case and leading/trailing whitespaces.
        return (login != null && this.login.trim().equalsIgnoreCase(login.trim()));
    }

    /**
     * Checks if the provided password matches the member's password.
     *
     * @param password the password to check
     * @return true if the provided password matches the member's password, false otherwise
     */
    public boolean isPassword(String password){
        return this.password.equals(password);
    }

    /**
     * Gets the login of the member.
     *
     * @return the login of the member
     */
    public String getLogin() {
        return login;
    }

    /**
     * Gets the profile description of the member.
     *
     * @return the profile description of the member
     */
    public String getProfile() {
        return profile;
    }

    public void addReview(Review review) {
        this.reviews.add(review);
    }

    public float getKarma() {
        return karma;
    }

    public void updateKarma() {
        if(!reviews.isEmpty()) {
            float sum = 0f;
            for (Review review : this.reviews) {
                sum += review.getMeanOpinions();
            }
            this.karma = (sum / this.reviews.size())/5f;
        }
    }

    /**
     * Returns a string representation of the member.
     *
     * @return a formatted string representation of the member, including login and profile
     */
    @Override
    public String toString() {
        // Return a formatted string representation of this member,
        // including its login and profile
        return String.format("login: %s\nprofile:\n%s\n", getLogin(), getProfile());
    }
}