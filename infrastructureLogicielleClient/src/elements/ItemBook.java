package elements;

import exceptions.BadEntryException;

/**
 * Represents a book item in the social network. Inherits from the Item class.
 */
public class ItemBook extends Item {

    /** The author of the book. */
    String author;

    /** The number of pages in the book. */
    int nbPages;

    /**
     * Constructs a new ItemBook with the specified title, kind, author, and number of pages.
     *
     * @param title the title of the book
     * @param kind the kind or genre of the book
     * @param author the author of the book
     * @param nbPages the number of pages in the book
     * @throws BadEntryException if any of the parameters are invalid
     */
    public ItemBook(String title, String kind, String author, int nbPages) throws BadEntryException {
        super(title, kind);
        if (author == null || author.trim().isEmpty()) {
            throw new BadEntryException("format de l'auteur incorrect");
        }
        if (nbPages < 0 || nbPages > 2000) {
            throw new BadEntryException("nombre de pages invalide (0 < x < 2000)");
        }
        // If all parameters are valid, assign them to the corresponding instance variables.
        this.author = author;
        this.nbPages = nbPages;
    }

    /**
     * Gets the author of the book.
     *
     * @return the author of the book
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Gets the number of pages in the book.
     *
     * @return the number of pages in the book
     */
    public int getNbPages() {
        return nbPages;
    }

    /**
     * Returns a string representation of the book item.
     *
     * @return a formatted string representation of the book item, including title, author, kind, and number of pages
     */
    @Override
    public String toString() {
        // Return a formatted string representation of this book item,
        // including its title, author, kind, and number of pages.
        return String.format("titre: %s\nauteur: %s\ngenre: %s\nnombre de pages: %d\nnote moyenne: %s\n", getTitle(), getAuthor(), getKind(), getNbPages(), Float.isNaN(getMeanReviews()) ? "pas encore de reviews" : getMeanReviews());
    }
}
