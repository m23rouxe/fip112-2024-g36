/**
 * This package contains classes representing tangible elements to the social network
 * <p>
 * It contains ItemBook and Member and their separate logic.
 * </p>
 *
 * @version 0.2
 */
package elements;