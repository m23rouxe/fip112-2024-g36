package tests;

import elements.Item;
import elements.ItemBook;
import elements.Review;
import exceptions.*;
import opinion.SocialNetwork;
import opinion.SocialNetworkPremium;

/**
 * Test class for reviewItemBook() method in ISocialNetwork.
 */
public class ReviewItemBookTest {

    /**
     * Tests the addition of a new review for a book on behalf of a specific member.
     * <p>
     * This method checks if adding a new review by a non-member raises a NotMemberException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error messages.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the book
     * @param mark         - mark given by the member
     * @param comment	   - comment given by the member
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */

	private static int reviewItemBookNotMemberTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		int nbItemReviews = sn.getItem(title, ItemBook.class).nbReviews(); //Number of reviews of a book before attempting to add
		try {
			sn.reviewItemBook(login,pwd,title,mark,comment); // Try to add a new review
			System.out.printf("Err %s : %s",testId,errorMessage); // Error message
			return 1; // Return 1 for failure
		}
		catch (NotMemberException e){ // Expected NotMemberException was thrown
			if (sn.getItem(title, ItemBook.class).nbReviews() != nbItemReviews) { //Number of reviews changed unexpectedly
				System.out.printf("Err %s : NotMember was thrown but the number of reviews was changed",testId);
				return 1;// Return 1 for failure
			}
			else {
				return 0; //Test passed
			}
		}
		catch (Exception e) { //Unexpected exception
			System.out.printf("Err %s : unexpected exception. %s" ,testId,e);
			e.printStackTrace();
			return 1;
		}	
	}
	
	/**
	 * Tests the addition of a review to a book with an unknown title in the ISocialNetwork.
     * <p>
     * This method checks if adding a review with a title not registered as a book's title
     * in the ISocialNetwork raises a NotItemException and doesn't change
     * the content of the ISocialNetwork. If the test fails, it displays an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the book
     * @param mark         - mark given by the member
     * @param comment      - comment given by the member
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int rewiewItemBookNotItemTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		try {
			sn.reviewItemBook(login,pwd,title,mark,comment); //Try to add a review
			System.out.printf("Err %s : %s\n",testId,errorMessage); //Error message
			return 1; //return 1 for failure
		}
		catch (NotItemException e) { //Expected NotItemException was thrown
				return 0; // Test passed
		}
		catch (Exception e) { //Unexpected exception
			System.out.printf("Err %s : unexpected exception. %s\n",testId,e);
			e.printStackTrace();
			return 1; //Test passed
		}
	}
	
	
	/**
	 * Tests the addition of a review with invalid parameters.
     * <p>
     * This method checks if adding a review with invalid parameters raises a BadEntryException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error message.
     * </p>
     *
     * @param sn      - the ISocialNetwork
     * @param login   - login of the user
     * @param pwd     - password of the user
     * @param title   - title of the book
     * @param mark    - mark given by the member
     * @param comment - comment given by the member
     * @param testId  - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int reviewItemBookBadEntryTest(SocialNetwork sn, String login,String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		int nbItemReviews = sn.getItem(title, ItemBook.class).nbReviews(); //Number of reviews of a book before attempting to add
        try {
        	
        	sn.reviewItemBook(login,pwd,title,mark,comment); //Try to add a review
			System.out.printf("Err %s : %s",testId,errorMessage); //Error message
			return 1; //return 1 for failure
		}
		catch (BadEntryException e) { //Expected BadEntryException was thrown
			if (sn.getItem(title, ItemBook.class).nbReviews() != nbItemReviews) { //Number of reviews changed unexpectedly
				System.out.printf("Err %s : NotItemException was thrown, but the number of reviews was changed\n",testId);
				return 1; // Return 1 for failure
			} 
            else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.printf("Err %s : unexpected exception. %s\n",testId, e);
            e.printStackTrace();
            return 1;
        }
    }
	
	
	/**
     * Tests the replacement of a review when a new one is added.
     * <p>
     * This method checks if adding a review when the same member has already given a review for this same book before
     * will delete the previous one.
     * </p>
     *
     * @param sn      - the ISocialNetwork
     * @param login   - login of the user
     * @param pwd     - password of the user
     * @param title   - title of the book
     * @param mark    - mark given by the member
     * @param comment - comment given by the member
     * @param testId  - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int rewiewItemBookReplaceTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId) {
		try {
			//Add the first review
			sn.reviewItemBook(login,pwd,title,mark,comment); //Add the first review
			int nbItemReviews = sn.getItem(title, ItemBook.class).nbReviews(); //Number of reviews of a book before attempting to add
			//Add the second review with different mark and comment
			float newMark = mark -1.0f;
			String newComment = comment + "-updated";
			sn.reviewItemBook(login,pwd,title,newMark,newComment);
			
			//check if the number of reviews remains the same
			if (sn.getItem(title, ItemBook.class).nbReviews() != nbItemReviews) {
				System.out.printf("Err %s : the number of reviews was changed to %S when it should have been %s\n",testId,sn.getItem(title, ItemBook.class).nbReviews(),nbItemReviews);
				return 1; //Test failed
			}
			
			//Check if the review has been updated
			Review currentReview = sn.getItem(title, ItemBook.class).getReviews().get(sn.getMember(login));
			if (currentReview.getMark() != newMark || !currentReview.getComment().equals(newComment)) {
				System.out.printf("Err %s: the review was not updated correctly\n",testId);
				return 1;//Test failed
			}
			else {
				return 0;//Test passed
			}
		}
		catch (Exception e) {
			System.out.printf("Err %s : unexpected exception %s\n",testId,e);
			e.printStackTrace();
			return 1;//Test failed due to exception
		}
	}
	
	
	
	/**
     * Tests the addition of a review with correct parameters.
     * <p>
     * This method checks if adding a review with correct parameters successfully adds
     * it to the ISocialNetwork.
     * </p>
     *
     * @param sn      - the ISocialNetwork
     * @param login   - login of the user
     * @param pwd     - password of the user
     * @param title   - title of the book
     * @param mark    - mark given by the member
     * @param comment - comment given by the member
     * @param testId  - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int reviewItemBookOKTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId) {
		int nbItemReviews = sn.getItem(title, ItemBook.class).nbReviews(); //Number of reviews of a book before attempting to add
		try {
			sn.reviewItemBook(login,pwd,title,mark,comment); //try to add a review to an item
			if (sn.getItem(title, ItemBook.class).nbReviews() != nbItemReviews +1) {//Number of reviews didn't increase as expected
				System.out.printf("Err %s : the number of reviews (%s) was not incremented\n",testId,nbItemReviews);
				return 1;//Return 1 for failure
			}
			else {
				return 0;//Test passed
			}
		}
		catch (Exception e) { //Unexpected exception
			System.out.printf("Err %s : unexpected exception %s\n",testId,e);
			e.printStackTrace();
			return 1; //Return 1 for failure
		}
	}

    /**
     * Main test method for reviewItemBook().
     * <p>
     * This method performs various tests:
     * <ul>
     * <li>Tests if incorrect member parameters cause reviewItemBook() to throw NotMember exception.</li>
     * <li>Tests if incorrect book parameters cause reviewItemBook() to throw NotItem exception.</li>
     * <li>Tests if incorrect member and book parameters cause reviewItemBook() to throw BadEntry exception.<li>
     * </ul>
     * </p>
     *
     * @return a summary of the performed tests
     */
    public static TestReport test(SocialNetwork sn) {
        try {
            sn.addMember("person", "mdpasse", "gentil.");
			sn.addItemBook("person", "mdpasse", "Lord Of The Rings", "fantasy", "JRR Martin", 600);
        } catch (Exception e) {
            System.out.println("Unexpected exception while using addMember or addItemBook");
        }

        int nbMembers = sn.nbMembers(); // Number of members in the ISocialNetwork 
        int nbFilms = sn.nbFilms(); // Number of films in the ISocialNetwork 0
		int nbBooks = sn.nbBooks();// Number of books in the ISocialNetwork 

        int nbTests = 0; // Total number of performed tests
        int nbErrors = 0; // Total number of failed tests

        System.out.println("Testing reviewItemBook()");

        // Test 1: NotMemberException tests
        nbTests += 4;
        nbErrors += reviewItemBookNotMemberTest(sn, null, "qsdfgh", "Lord Of The Rings", 4.5f,"Really nice book","6.1","reviewItemBook() allows null login to add reviews");
        nbErrors += reviewItemBookNotMemberTest(sn,"jonathan",null, "Lord Of The Rings", 4.5f,"Really nice book","6.2","reviewItemBook() allows users with null passwords to add reviews");
        nbErrors += reviewItemBookNotMemberTest(sn, "jonathan", "qsdfgh", "Lord Of The Rings", 4.5f,"really nice book","6.3", "reviewItemBook() allows non-existent users to add reviews");
        nbErrors += reviewItemBookNotMemberTest(sn, "person", "motdepasse", "Lord Of The Rings", 4.2f,"too short","6.4", "reviewItemBook() allows users with incorrect passwords to add reviews");
        
        // Test 2: NotItemException tests
        nbTests += 2;
        nbErrors += rewiewItemBookNotItemTest(sn,"person","mdpasse","The Giver",4.5f,"Really nice book","7.1","reviewItemBook() allows adding a review to non-existent titles");
        nbErrors += rewiewItemBookNotItemTest(sn, "person", "mdpasse", null, 5.0f, "yeah nice", "7.2", "reviewItemBook() allows adding a review to null referenced titles");

        // Test 3: BadEntryException tests
        nbTests += 5;
        nbErrors += reviewItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", 4.5f,null,"5.3", "reviewItemBook() allows reviews with null comments to be added");
        nbErrors += reviewItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", 4.5f,"","5.4", "reviewItemBook() allows reviews with empty comments to be added");
        nbErrors += reviewItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", 4.5f,"      ","5.5", "reviewItemBook() allows reviews with comments made of spaces to be added");
        nbErrors += reviewItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", -1.5f,"Really nice book","5.1", "reviewItemBook() allows reviews with marks less than 0.0 to be added");
        nbErrors += reviewItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", 6.5f,"Really nice book","5.2", "reviewItemBook() allows reviews with marks greater than 5.0 to be added");
                  
        
        // Test 4: Replace tests
		nbTests++;
        nbErrors += rewiewItemBookReplaceTest(sn, "person", "mdpasse", "Lord Of The Rings",4.5f,"Really nice book","8.1");
        
        
        
        // Check if ISocialNetwork was modified
        nbTests += 2;
        if (nbFilms != sn.nbFilms()) {
            System.out.println("Error: the number of books was unexpectedly changed by addItemBook()");
            nbErrors++;
        }
        if (nbMembers != sn.nbMembers()) {
            System.out.println("Error: the number of members was unexpectedly changed by addItemBook()");
            nbErrors++;
        }

        // Display final state of the ISocialNetwork
        System.out.printf("Final state of the social network: %s\n", sn);

        // Print test summary
        try {
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.printf("ReviewItemBookTest: %s\n", tr);
            return tr;
        } catch (NotTestReportException e) {
            System.out.println("Unexpected error in AddMemberTest test code - Can't return valuable test results");
            return null;
        }
    }

    /**
     * Launches test().
     *
     * @param args not used
     */
	public static void main(String[] args) {
		SocialNetwork sn = new SocialNetwork();
		test(sn);
		SocialNetworkPremium snp = new SocialNetworkPremium();
		test(snp);
	}
}	