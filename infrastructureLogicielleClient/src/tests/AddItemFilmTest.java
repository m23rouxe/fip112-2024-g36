package tests;

import exceptions.*;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;
import opinion.SocialNetworkPremium;

/**
 * Test class for addItemFilm() method in ISocialNetwork.
 */
public class AddItemFilmTest {

    /**
     * Tests the addition of a film when the user is not a member.
     * <p>
     * This method checks if adding a new film by a non-member raises a NotMemberException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the film
     * @param kind         - genre of the film
     * @param director     - director of the film
     * @param scenarist    - scenarist of the film
     * @param duration     - duration of the film in minutes
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemFilmNotMemberTest(ISocialNetwork sn, String login,String pwd, String title, String kind, String director, String scenarist, int duration, String testId, String errorMessage) {
        int nbItemFilms = sn.nbFilms(); // Number of films before attempting to add
        try {
            sn.addItemFilm(login, pwd, title, kind, director, scenarist, duration); // Try to add a film
            System.out.println("Err " + testId + " : " + errorMessage); // Error message
            return 1; // Return 1 for failure
        } catch (NotMemberException e) { // Expected NotMemberException was thrown
            if (sn.nbFilms() != nbItemFilms) { // Number of films changed unexpectedly
                System.out.println("Err " + testId + " : NotMember was thrown but the number of films was changed");
                return 1;
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception. " + e);
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * Tests the addition of a film with invalid parameters.
     * <p>
     * This method checks if adding a film with invalid parameters raises a BadEntryException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the film
     * @param kind         - genre of the film
     * @param director     - director of the film
     * @param scenarist    - scenarist of the film
     * @param duration     - duration of the film in minutes
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemFilmBadEntryTest(ISocialNetwork sn, String login, String pwd, String title, String kind, String director, String scenarist, int duration, String testId, String errorMessage) {
        int nbItemFilms = sn.nbFilms(); // Number of films before attempting to add
        try {
            sn.addItemFilm(login, pwd, title, kind, director, scenarist, duration); // Try to add a film
            System.out.println("Err " + testId + " : " + errorMessage); // Error message
            return 1; // Return 1 for failure
        } catch (BadEntryException e) { // Expected BadEntryException was thrown
            if (sn.nbFilms() != nbItemFilms) { // Number of films changed unexpectedly
                System.out.println("Err " + testId + " : BadEntry was thrown but the number of films was changed");
                return 1;
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception. " + e);
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * Tests the addition of a film with already existing title.
     * <p>
     * This method checks if adding a film with a title that already exist
     * in the ISocialNetwork raises an ItemFilmAlreadyExistsException and doesn't change
     * the content of the ISocialNetwork. If the test fails, it displays an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the film
     * @param kind         - genre of the film
     * @param director     - director of the film
     * @param scenarist    - scenarist of the film
     * @param duration     - duration of the film in minutes
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemFilmAlreadyExistsTest(ISocialNetwork sn, String login, String pwd, String title, String kind, String director, String scenarist, int duration, String testId, String errorMessage) {
        int nbItemFilms = sn.nbFilms(); // Number of films before attempting to add
        try {
            sn.addItemFilm(login, pwd, title, kind, director, scenarist, duration); // Try to add a film
            System.out.println("Err " + testId + " : " + errorMessage); // Error message
            return 1; // Return 1 for failure
        } catch (ItemFilmAlreadyExistsException e) { // Expected ItemFilmAlreadyExistsException was thrown
            if (sn.nbFilms() != nbItemFilms) { // Number of films changed unexpectedly
                System.out.println("Err " + testId + " : ItemFilmAlreadyExists was thrown, but the number of films was changed");
                return 1;
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception. " + e);
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * Tests the addition of a film with correct parameters.
     * <p>
     * This method checks if adding a film with correct parameters successfully adds
     * it to the ISocialNetwork.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the film
     * @param kind         - genre of the film
     * @param director     - director of the film
     * @param scenarist    - scenarist of the film
     * @param duration     - duration of the film in minutes
     * @param testId       - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemFilmOKTest(ISocialNetwork sn, String login, String pwd, String title, String kind, String director, String scenarist, int duration, String testId) {
        int nbItemFilms = sn.nbFilms(); // Number of films before attempting to add
        try {
            sn.addItemFilm(login, pwd, title, kind, director, scenarist, duration); // Try to add a film
            if (sn.nbFilms() != nbItemFilms + 1) { // Number of films didn't increase as expected
                System.out.println("Err " + testId + " : the number of films (" + nbItemFilms + ") was not incremented");
                return 1; // Return 1 for failure
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception " + e);
            e.printStackTrace();
            return 1; // Return 1 for failure
        }
    }

    /**
     * Main test method for addItemFilm().
     * <p>
     * This method performs various tests:
     * <ul>
     * <li>Tests if incorrect member parameters cause addItemFilm() to throw NotMember exception.</li>
     * <li>Tests if incorrect film parameters cause addItemFilm() to throw BadEntry exception.</li>
     * <li>Tests if adding already existing films cause addItemFilm() to throw ItemFilmAlreadyExists exception.</li>
     * </ul>
     * </p>
     *
     * @return a summary of the performed tests
     */
    public static TestReport test(SocialNetwork sn) {
        try {
            sn.addMember("person", "mdpasse", "gentil.");
        } catch (Exception e) {
            System.out.println("Unexpected exception while using addMember");
        }

        int nbMembers = sn.nbMembers(); // Number of members in the ISocialNetwork 1
        int nbBooks = sn.nbBooks(); // Number of films in the ISocialNetwork 0

        int nbTests = 0; // Total number of performed tests
        int nbErrors = 0; // Total number of failed tests

        System.out.println("Testing addItemFilm()");

        // Test 1: NotMemberException tests
        nbTests++;
        nbErrors += addItemFilmNotMemberTest(sn, null, "qsdfgh", "Top Gun", "action", "Tony Scott","Jim Cash", 109,"11.1", "addItemFilm() allows null login to add films");
        nbErrors += addItemFilmNotMemberTest(sn, "jonathan", "qsdfgh", "Top Gun", "action", "Tony Scott", "Jim Cash", 109,"11.2", "addItemFilm() allows non-existent users to add films");
        nbTests++;
        nbErrors += addItemFilmNotMemberTest(sn, "person", null, "Top Gun", "action", "Tony Scott", "Jim Cash", 109,"11.3", "addItemFilm() allows users with null passwords to add films");
        nbErrors += addItemFilmNotMemberTest(sn, "person", "motdepasse", "Top Gun", "action", "Tony Scott", "Jim Cash", 109,"11.4", "addItemFilm() allows users with incorrect passwords to add films");

        // Test 2: BadEntryException tests
        nbTests += 8;
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", null, "action", "Tony Scott", "Jim Cash", 109,"10.1", "addItemFilm() allows films with null titles to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "", "action", "Tony Scott", "Jim Cash", 109,"10.2", "addItemFilm() allows films with empty titles to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "      ", "action","Tony Scott", "Jim Cash", 109, "10.3", "addItemFilm() allows films with titles made of spaces to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", null, "Tony Scott","Jim Cash",109, "10.4", "addItemFilm() allows films with null kinds to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "", "Tony Scott","Jim Cash",109, "10.5", "addItemFilm() allows films with empty kinds to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "		", "Tony Scott","Jim Cash",109, "10.6", "addItemFilm() allows films with kinds made of spaces to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun","action",null,"Jim Cash", 109, "10.7", "addItemFilm() allows films with null directors to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "", "Tony Scott","Jim Cash",109, "10.8", "addItemFilm() allows films with empty directors to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "		", "Tony Scott","Jim Cash",109, "10.9", "addItemFilm() allows films with directors made of spaces to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun","action","Tony Scott", null, 109, "10.10", "addItemFilm() allows films with null scriptwriters to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "action","Tony Scott", "", 109, "10.11", "addItemFilm() allows films with an empty scriptwriter to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "action", "Tony Scott","		", -90, "10.12", "addItemFilm() allows films with scriptwriter made of spaces to be added");    
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "action","Tony Scott", "Jim Cash", 0, "10.13", "addItemFilm() allows films with a duration of 0 minute to be added");
        nbErrors += addItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", "action", "Tony Scott","Jim Cash", -90, "10.14", "addItemFilm() allows films with a negative duration to be added");
       
        
        // Test 3: ItemFilmAlreadyExistsException tests
        nbTests += 3;
        nbErrors += addItemFilmOKTest(sn, "person", "mdpasse", "Top Gun", "action","Tony Scott","Jim Cash", 109, "noId");
        nbErrors += addItemFilmOKTest(sn, "person", "mdpasse", "Avatar", "sci-fi","James Cameron","James Cameron", 162, "noId");
        nbErrors += addItemFilmOKTest(sn, "person", "mdpasse", "Dirty Dancing", "drame musical", "Emile Ardolino","Eleanor Bergstein", 100, "noId");

        nbTests += 2;
        nbErrors += addItemFilmAlreadyExistsTest(sn, "person", "mdpasse", "Top Gun", "action","Tony Scott","Jim Cash", 109, "12.1", "addItemFilm() allows films with existing title to be added");
        nbErrors += addItemFilmAlreadyExistsTest(sn, "person", "mdpasse", "   Top Gun    ", "action","Tony Scott", "Jim Cash", 109, "12.2", "addItemFilm() allows films with existing title to be added with leading/surrounding blanks");
        nbErrors += addItemFilmAlreadyExistsTest(sn, "person", "mdpasse", "Top 			Gun", "action","Tony Scott","Jim Cash", 109, "12.3", "addItemFilm() allows films with existing title to be added with leading spaces");
        nbErrors += addItemFilmAlreadyExistsTest(sn, "person", "mdpasse", "TOP gun", "action","Tony Scott", "Jim Cash", 109, "12.4", "addItemFilm() allows films with existing title to be added by taking into account the case-sensitivity");

        // Check if ISocialNetwork was modified
        nbTests += 2;
        if (nbBooks != sn.nbBooks()) {
            System.out.println("Error: the number of films was unexpectedly changed by addItemFilm()");
            nbErrors++;
        }
        if (nbMembers != sn.nbMembers()) {
            System.out.println("Error: the number of members was unexpectedly changed by addItemFilm()");
            nbErrors++;
        }

        // Display final state of the ISocialNetwork
        System.out.println("Final state of the social network: " + sn);

        // Print test summary
        try {
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("AddItemFilmTest: " + tr);
            return tr;
        } catch (NotTestReportException e) {
            System.out.println("Unexpected error in AddMemberTest test code - Can't return valuable test results");
            return null;
        }
    }

    /**
     * Launches test().
     *
     * @param args not used
     */
    public static void main(String[] args) {
        SocialNetwork sn = new SocialNetwork();
        test(sn);
        SocialNetworkPremium snp = new SocialNetworkPremium();
        test(snp);
    }
}