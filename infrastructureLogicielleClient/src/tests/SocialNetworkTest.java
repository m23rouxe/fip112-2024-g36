package tests;


import opinion.SocialNetwork;

/**
 * This class launches a test suite for the SocialNetwork
 * @author B. Prou, GO
 * @version V2.0 - April 2018
 * 
 */
public class SocialNetworkTest {

	/**
	 * @param args not used
	 */
	public static void main(String[] args) {

		try {
			System.out.println("\n\n****************************************INITIALISATION***************************************\n");
			TestReport testSuiteReport = new TestReport(0,0);
			TestReport tr;
			tr = InitTest.test();
			testSuiteReport.add(tr);
			//
			System.out.println("\n\n***************************************ADDMEMBER***************************************\n");
			SocialNetwork sn = new SocialNetwork();
			SocialNetwork snp = new SocialNetwork();
			tr = AddMemberTest.test(sn);
			testSuiteReport.add(tr);
			System.out.println("\nPremium version : \n");
			tr = AddMemberTest.test(snp);
			testSuiteReport.add(tr);
			System.out.println("\n\n***************************************ADDITEMBOOK*******************************************\n");
			//
			sn = new SocialNetwork();
			snp = new SocialNetwork();
			tr = AddItemBookTest.test(sn);
			testSuiteReport.add(tr);
			System.out.println("\nPremium version : \n");
			tr = AddItemBookTest.test(snp);
			testSuiteReport.add(tr);
			System.out.println("\n\n****************************************ADDITEMFILM*****************************************\n");
			//
			sn = new SocialNetwork();
			snp = new SocialNetwork();
			tr = AddItemFilmTest.test(sn);
			testSuiteReport.add(tr);
			System.out.println("\nPremium version : \n");
			tr = AddItemFilmTest.test(snp);
			testSuiteReport.add(tr);
			//
			System.out.println("\n\n******************************************CONSULTITEMS**************************************\n");
			sn = new SocialNetwork();
			snp = new SocialNetwork();
			tr = ConsultItemsTest.test(sn);
			testSuiteReport.add(tr);
			System.out.println("\nPremium version : \n");
			tr = ConsultItemsTest.test(snp);
			testSuiteReport.add(tr);
			//
			System.out.println("\n\n******************************************REVIEWITEMBOOK****************************************\n");
			sn = new SocialNetwork();
			snp = new SocialNetwork();
			tr = ReviewItemBookTest.test(sn);
			testSuiteReport.add(tr);
			System.out.println("\nPremium version : \n");
			tr = ReviewItemBookTest.test(snp);
			testSuiteReport.add(tr);
			//
			System.out.println("\n\n*****************************************REVIEWITEMFILM*************************************\n");
			sn = new SocialNetwork();
			snp = new SocialNetwork();
			tr = ReviewItemFilmTest.test(sn);
			testSuiteReport.add(tr);
			System.out.println("\nPremium version : \n");
			tr = ReviewItemFilmTest.test(snp);
			testSuiteReport.add(tr);

			System.out.println("\n\n************************************REVIEWOPINION (Premium Only)*****************************\n");
			tr = ReviewOpinionTest.test();
			testSuiteReport.add(tr);
			System.out.println("\n\n**********************************************************************************************\n");
			
			// End of the test suite : give some feedback to the tester
			System.out.println("Global tests results :   \n" + testSuiteReport);
		}
		catch (Exception e) {System.out.println("ERROR : Some exception was throw unexpectedly");}
		
	}

}
