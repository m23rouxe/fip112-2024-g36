package tests;

import elements.Item;
import elements.ItemBook;
import elements.ItemFilm;
import elements.Review;
import exceptions.*;
import opinion.SocialNetwork;
import opinion.SocialNetworkPremium;

/**
 * Test class for reviewItemFilm() method in ISocialNetwork.
 */
public class ReviewItemFilmTest {

    /**
     * Tests the addition of a new review for a film on behalf of a specific member.
     * <p>
     * This method checks if adding a new review by a non-member raises a NotMemberException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error messages.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the film
     * @param mark         - mark given by the member
     * @param comment	   - comment given by the member
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */

	private static int reviewItemFilmNotMemberTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		int nbItemReviews = sn.getItem(title, ItemFilm.class).nbReviews(); //Number of reviews of a book before attempting to add
		try {
			sn.reviewItemBook(login,pwd,title,mark,comment); // Try to add a new review
			System.out.printf("Err %s : %s",testId,errorMessage); // Error message
			return 1; // Return 1 for failure
		}
		catch (NotMemberException e){ // Expected NotMemberException was thrown
			if (sn.getItem(title, ItemFilm.class).nbReviews() != nbItemReviews) { //Number of reviews changed unexpectedly
				System.out.printf("Err %s : NotMember was thrown but the number of reviews was changed",testId);
				return 1;// Return 1 for failure
			}
			else {
				return 0; //Test passed
			}
		}
		catch (Exception e) { //Unexpected exception
			System.out.printf("Err %s : unexpected exception. %s" ,testId,e);
			e.printStackTrace();
			return 1;
		}	
	}
	
	/**
	 * Tests the addition of a review to a book with an unknown title in the ISocialNetwork.
     * <p>
     * This method checks if adding a review with a title not registered as a book's title
     * in the ISocialNetwork raises a NotItemException and doesn't change
     * the content of the ISocialNetwork. If the test fails, it displays an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the book
     * @param mark         - mark given by the member
     * @param comment      - comment given by the member
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int rewiewItemFilmNotItemTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		try {
			sn.reviewItemBook(login,pwd,title,mark,comment); //Try to add a review
			System.out.printf("Err %s : %s\n",testId,errorMessage); //Error message
			return 1; //return 1 for failure
		}
		catch (NotItemException e) { //Expected NotItemException was thrown
				return 0; // Test passed
		}
		catch (Exception e) { //Unexpected exception
			System.out.printf("Err %s : unexpected exception. %s\n",testId,e);
			e.printStackTrace();
			return 1; //Test passed
		}
	}
	
	
	/**
	 * Tests the addition of a review with invalid parameters.
     * <p>
     * This method checks if adding a review with invalid parameters raises a BadEntryException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error message.
     * </p>
     *
     * @param sn      - the ISocialNetwork
     * @param login   - login of the user
     * @param pwd     - password of the user
     * @param title   - title of the book
     * @param mark    - mark given by the member
     * @param comment - comment given by the member
     * @param testId  - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int reviewItemFilmBadEntryTest(SocialNetwork sn, String login,String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		int nbItemReviews = sn.getItem(title, ItemFilm.class).nbReviews(); //Number of reviews of a film before attempting to add
        try {
        	
        	sn.reviewItemFilm(login,pwd,title,mark,comment); //Try to add a review
			System.out.printf("Err %s : %s",testId,errorMessage); //Error message
			return 1; //return 1 for failure
		}
		catch (BadEntryException e) { //Expected BadEntryException was thrown
			if (sn.getItem(title, ItemFilm.class).nbReviews() != nbItemReviews) { //Number of reviews changed unexpectedly
				System.out.printf("Err %s : NotItemException was thrown, but the number of reviews was changed\n",testId);
				return 1; // Return 1 for failure
			} 
            else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.printf("Err %s : unexpected exception. %s\n",testId, e);
            e.printStackTrace();
            return 1;
        }
    }
	
	
	/**
     * Tests the replacement of a review when a new one is added.
     * <p>
     * This method checks if adding a review when the same member has already given a review for this same book before
     * will delete the previous one.
     * </p>
     *
     * @param sn      - the ISocialNetwork
     * @param login   - login of the user
     * @param pwd     - password of the user
     * @param title   - title of the book
     * @param mark    - mark given by the member
     * @param comment - comment given by the member
     * @param testId  - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int rewiewItemFilmReplaceTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId) {
		try {
			//Add the first review
			sn.reviewItemFilm(login,pwd,title,mark,comment); //Add the first review
			int nbItemReviews = sn.getItem(title, ItemFilm.class).nbReviews(); //Number of reviews of a film before attempting to add
			//Add the second review with different mark and comment
			float newMark = mark -1.0f;
			String newComment = comment + "-updated";
			sn.reviewItemFilm(login,pwd,title,newMark,newComment);
			
			//check if the number of reviews remains the same
			if (sn.getItem(title, ItemFilm.class).nbReviews() != nbItemReviews) {
				System.out.printf("Err %s : the number of reviews was changed to %S when it should have been %s\n",testId,sn.getItem(title, ItemFilm.class).nbReviews(),nbItemReviews);
				return 1; //Test failed
			}
			
			//Check if the review has been updated
			Review currentReview = sn.getItem(title, ItemFilm.class).getReviews().get(sn.getMember(login));
			if (currentReview.getMark() != newMark || !currentReview.getComment().equals(newComment)) {
				System.out.printf("Err %s: the review was not updated correctly\n",testId);
				return 1;//Test failed
			}
			else {
				return 0;//Test passed
			}
		}
		catch (Exception e) {
			System.out.printf("Err %s : unexpected exception %s\n",testId,e);
			e.printStackTrace();
			return 1;//Test failed due to exception
		}
	}
	
	
	
	/**
     * Tests the addition of a review with correct parameters.
     * <p>
     * This method checks if adding a review with correct parameters successfully adds
     * it to the ISocialNetwork.
     * </p>
     *
     * @param sn      - the ISocialNetwork
     * @param login   - login of the user
     * @param pwd     - password of the user
     * @param title   - title of the book
     * @param mark    - mark given by the member
     * @param comment - comment given by the member
     * @param testId  - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
	
	private static int reviewItemFilmOKTest(SocialNetwork sn, String login, String pwd, String title, float mark, String comment, String testId) {
		int nbItemReviews = sn.getItem(title, ItemFilm.class).nbReviews(); //Number of reviews of a film before attempting to add
		try {
			sn.reviewItemFilm(login,pwd,title,mark,comment); //try to add a review to an item
			if (sn.getItem(title, ItemFilm.class).nbReviews() != nbItemReviews +1) {//Number of reviews didn't increase as expected
				System.out.printf("Err %s : the number of reviews (%s) was not incremented\n",testId,nbItemReviews);
				return 1;//Return 1 for failure
			}
			else {
				return 0;//Test passed
			}
		}
		catch (Exception e) { //Unexpected exception
			System.out.printf("Err %s : unexpected exception %s\n",testId,e);
			e.printStackTrace();
			return 1; //Return 1 for failure
		}
	}

    /**
     * Main test method for reviewItemFilm().
     * <p>
     * This method performs various tests:
     * <ul>
     * <li>Tests if incorrect member parameters cause reviewItemFilm() to throw NotMember exception.</li>
     * <li>Tests if incorrect film parameters cause reviewItemFilm() to throw NotItem exception.</li>
     * <li>Tests if incorrect member and film parameters cause reviewItemFilm() to throw BadEntry exception.<li>
     * </ul>
     * </p>
     *
     * @return a summary of the performed tests
     */
    public static TestReport test(SocialNetwork sn) {
        try {
            sn.addMember("person", "mdpasse", "gentil.");
			sn.addItemFilm("person", "mdpasse", "Top Gun", "action","Tony Scott", "Jim Cash", 109);
        } catch (Exception e) {
            System.out.println("Unexpected exception while using addMember or addItemFilm");
        }

        int nbMembers = sn.nbMembers(); // Number of members in the ISocialNetwork 
        int nbFilms = sn.nbFilms(); // Number of films in the ISocialNetwork 0
		int nbBooks = sn.nbBooks();// Number of books in the ISocialNetwork 

        int nbTests = 0; // Total number of performed tests
        int nbErrors = 0; // Total number of failed tests

        System.out.println("Testing reviewItemFilm()");

        // Test 1: NotMemberException tests
        nbTests += 4;
        nbErrors += reviewItemFilmNotMemberTest(sn, "jonathan", "qsdfgh", "Top Gun", 5.0f,"Best film","14.1", "reviewItemFilm() allows non-existent users to add reviews");
        nbErrors += reviewItemFilmNotMemberTest(sn, "person", "motdepasse", "Top Gun", 5.0f,"Best film","14.2", "reviewItemFilm() allows users with incorrect passwords to add reviews");
        nbErrors += reviewItemFilmNotMemberTest(sn, null, "qsdfgh", "Top Gun", 5.0f,"Best film","14.3","reviewItemFilm() allows null login to add reviews");
        nbErrors += reviewItemFilmNotMemberTest(sn,"jonathan",null, "Top Gun", 5.0f,"Best film","14.4","reviewItemFilm() allows users with null passwords to add reviews");
      
        // Test 2: NotItemException tests
        nbTests += 2;
        nbErrors += rewiewItemFilmNotItemTest(sn,"person","mdpasse","The Giver",4.5f,"Really nice film","15.1","reviewItemFilm() allows adding a review to non-existent titles");
        nbErrors += rewiewItemFilmNotItemTest(sn, "person", "mdpasse", null, 5.0f, "Best film", "15.2", "reviewItemFilm() allows adding a review to null referenced titles");

        // Test 3: BadEntryException tests
        nbTests += 5;
        nbErrors += reviewItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", -1.5f,"bad film","13.1", "reviewItemFilm() allows reviews with marks less than 0.0 to be added");
        nbErrors += reviewItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", 6.5f,"Really nice film","13.2", "reviewItemFilm() allows reviews with marks greater than 5.0 to be added");
        nbErrors += reviewItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", 5.0f,null,"13.3", "reviewItemFilm() allows reviews with null comments to be added");
        nbErrors += reviewItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", 5.0f,"","13.4", "reviewItemFilm() allows reviews with marks greater than 5.0 to be added");
        nbErrors += reviewItemFilmBadEntryTest(sn, "person", "mdpasse", "Top Gun", 5.0f,"    ","13.5", "reviewItemFilm() allows reviews with null comments to be added");
         
        // Test 4: Replace tests
		nbTests++;
        nbErrors += rewiewItemFilmReplaceTest(sn, "person", "mdpasse", "Top Gun",5.0f,"Best film","16.1");
        
        
        
        // Check if ISocialNetwork was modified
        nbTests += 2;
        if (nbBooks != sn.nbBooks()) {
            System.out.println("Error: the number of books was unexpectedly changed by addItemFilm()");
            nbErrors++;
        }
        if (nbMembers != sn.nbMembers()) {
            System.out.println("Error: the number of members was unexpectedly changed by addItemFilm()");
            nbErrors++;
        }

        // Display final state of the ISocialNetwork
        System.out.printf("Final state of the social network: %s\n", sn);

        // Print test summary
        try {
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.printf("ReviewItemFilmTest: %s\n", tr);
            return tr;
        } catch (NotTestReportException e) {
            System.out.println("Unexpected error in AddMemberTest test code - Can't return valuable test results");
            return null;
        }
    }

    /**
     * Launches test().
     *
     * @param args not used
     */
	public static void main(String[] args) {
		SocialNetwork sn = new SocialNetwork();
		test(sn);
		SocialNetworkPremium snp = new SocialNetworkPremium();
		test(snp);
	}
}
