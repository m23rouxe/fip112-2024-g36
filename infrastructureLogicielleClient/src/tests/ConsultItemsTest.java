package tests;

import elements.ItemBook;
import exceptions.BadEntryException;
import exceptions.NotTestReportException;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;
import opinion.SocialNetworkPremium;

import java.util.LinkedList;

/**
 * Test class for consultItems() method in ISocialNetwork.
 */
public class ConsultItemsTest {
	
	/**
     * Tests the consultation of a book with invalid titles.
     * <p>
     * This method checks if consulting a book with an invalid title raises a BadEntryException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param title        - title of the book
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
    public static int consultItemsBadEntryTest(ISocialNetwork sn, String title, String testId, String errorMessage) {
    	int nbItemBooks = sn.nbBooks();//Number of items
        try {
        	LinkedList<String> items = sn.consultItems(title);
        	System.out.printf("Err %s  : %s\n",testId, errorMessage); // Error message
            return 1; // Return 1 for failure
        }
        catch (BadEntryException e){ //Expected BadEntryException was thrown
        	if (sn.nbBooks() != nbItemBooks) { //Check if the number of items has not changed
        		System.out.printf("Err %s : the number of items changed.\n",testId);
        		return 1; //Return 1 for failure
        	}
        	return 0;
        }
        catch (Exception e) { //Unexpected exception was thrown
        	System.out.printf("Err %s : unexpected exception: %s\n",testId,e);
        	return 1; //Return 1 for failure
        }
    }
	
	
    /**
     * Main test method for consultItems().
     * <p>
     * This method performs various tests:
     * <ul>
     * <li>Tests if incorrect title parameters cause consultItems() to throw BadEntry exception.</li>
     * </ul>
     * </p>
     *
     * @return a summary of the performed tests
     */
    
    public static TestReport test(SocialNetwork sn) {
        try {
            sn.addMember("person", "mdpasse", "gentil.");
			sn.addItemBook("person", "mdpasse", "Lord Of The Rings", "fantasy", "JRR Martin", 600);
            sn.reviewItemBook("person", "mdpasse", "Lord Of The Rings", (float)5.0, "Greatest fantasy book ever");
        } catch (Exception e) {
            System.out.println("Unexpected exception while using addMember, addItemBook or reviewItemBook");
        }

        int nbMembers = sn.nbMembers(); // Number of members in the ISocialNetwork 
        int nbFilms = sn.nbFilms(); // Number of films in the ISocialNetwork 0
		int nbBooks = sn.nbBooks();// Number of books in the ISocialNetwork  1
        int nbReviews = sn.getItem("Lord Of The Rings", ItemBook.class).nbReviews(); // Number of reviews for the book

        int nbTests = 0; // Total number of performed tests
        int nbErrors = 0; // Total number of failed tests

        System.out.println("Testing ConsultItemsTest()");

        // Test 1: NotMemberException tests
        nbTests+=3;
        nbErrors += consultItemsBadEntryTest(sn,null,"9.1","consultItems() allows null title");
        nbErrors += consultItemsBadEntryTest(sn,"","9.2","consultItems() allows empty title");
        nbErrors += consultItemsBadEntryTest(sn,"     ","9.3", "consultItems() allows title made of spaces");

        nbTests++;
        try {
            if(nbReviews != sn.consultItems("Lord Of The Rings").size()){
                nbErrors++;
                System.out.println("number of strings returned by consultItems incorrect");
            }
        }catch (Exception e){
            nbErrors++;
            System.out.println("unexpected Exception occured");
        }

        
        // Check if ISocialNetwork was modified
        nbTests += 2;
        if (nbFilms != sn.nbFilms()) {
            System.out.println("Error: the number of books was unexpectedly changed by addItemBook()");
            nbErrors++;
        }
        if (nbMembers != sn.nbMembers()) {
            System.out.println("Error: the number of members was unexpectedly changed by addItemBook()");
            nbErrors++;
        }

        // Display final state of the ISocialNetwork
        System.out.printf("Final state of the social network: %s\n",sn);

        // Print test summary
        try {
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.printf("ConsultItemsTest: %s\n", tr);
            return tr;
        } catch (NotTestReportException e) {
            System.out.println("Unexpected error in AddMemberTest test code - Can't return valuable test results");
            return null;
        }
    }

    /**
     * Launches test().
     *
     * @param args not used
     */
    public static void main(String[] args) {
        SocialNetwork sn = new SocialNetwork();
        test(sn);
        SocialNetworkPremium snp = new SocialNetworkPremium();
        test(snp);
    }

     
}