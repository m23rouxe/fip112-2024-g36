package tests;

import exceptions.*;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;
import opinion.SocialNetworkPremium;

/**
 * Test class for addItemBook() method in ISocialNetwork.
 */
public class AddItemBookTest {

    /**
     * Tests the addition of a book when the user is not a member.
     * <p>
     * This method checks if adding a new book by a non-member raises a NotMemberException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the book
     * @param kind         - genre of the book
     * @param author       - author of the book
     * @param nbPages      - number of pages in the book
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemBookNotMemberTest(ISocialNetwork sn, String login,
                                                String pwd, String title, String kind, String author, int nbPages, String testId, String errorMessage) {
        int nbItemBooks = sn.nbBooks(); // Number of books before attempting to add
        try {
            sn.addItemBook(login, pwd, title, kind, author, nbPages); // Try to add a book
            System.out.println("Err " + testId + " : " + errorMessage); // Error message
            return 1; // Return 1 for failure
        } catch (NotMemberException e) { // Expected NotMemberException was thrown
            if (sn.nbBooks() != nbItemBooks) { // Number of books changed unexpectedly
                System.out.println("Err " + testId + " : NotMember was thrown but the number of books was changed");
                return 1;
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception. " + e);
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * Tests the addition of a book with invalid parameters.
     * <p>
     * This method checks if adding a book with invalid parameters raises a BadEntryException
     * and doesn't change the content of the ISocialNetwork. If the test fails, it displays
     * an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the book
     * @param kind         - genre of the book
     * @param author       - author of the book
     * @param nbPages      - number of pages in the book
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemBookBadEntryTest(ISocialNetwork sn, String login,
                                               String pwd, String title, String kind, String author, int nbPages, String testId, String errorMessage) {
        int nbItemBooks = sn.nbBooks(); // Number of books before attempting to add
        try {
            sn.addItemBook(login, pwd, title, kind, author, nbPages); // Try to add a book
            System.out.println("Err " + testId + " : " + errorMessage); // Error message
            return 1; // Return 1 for failure
        } catch (BadEntryException e) { // Expected BadEntryException was thrown
            if (sn.nbBooks() != nbItemBooks) { // Number of books changed unexpectedly
                System.out.println("Err " + testId + " : BadEntry was thrown but the number of books was changed");
                return 1;
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception. " + e);
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * Tests the addition of a book with already existing title and author.
     * <p>
     * This method checks if adding a book with a title and author that already exist
     * in the ISocialNetwork raises an ItemBookAlreadyExistsException and doesn't change
     * the content of the ISocialNetwork. If the test fails, it displays an error message.
     * </p>
     *
     * @param sn           - the ISocialNetwork
     * @param login        - login of the user
     * @param pwd          - password of the user
     * @param title        - title of the book
     * @param kind         - genre of the book
     * @param author       - author of the book
     * @param nbPages      - number of pages in the book
     * @param testId       - ID of the test
     * @param errorMessage - the error message to display if the test fails
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemBookAlreadyExistsTest(ISocialNetwork sn, String login,
                                                    String pwd, String title, String kind, String author, int nbPages, String testId, String errorMessage) {
        int nbItemBooks = sn.nbBooks(); // Number of books before attempting to add
        try {
            sn.addItemBook(login, pwd, title, kind, author, nbPages); // Try to add a book
            System.out.println("Err " + testId + " : " + errorMessage); // Error message
            return 1; // Return 1 for failure
        } catch (ItemBookAlreadyExistsException e) { // Expected ItemBookAlreadyExistsException was thrown
            if (sn.nbBooks() != nbItemBooks) { // Number of books changed unexpectedly
                System.out.println("Err " + testId + " : ItemBookAlreadyExists was thrown, but the number of books was changed");
                return 1;
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception. " + e);
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * Tests the addition of a book with correct parameters.
     * <p>
     * This method checks if adding a book with correct parameters successfully adds
     * it to the ISocialNetwork.
     * </p>
     *
     * @param sn      - the ISocialNetwork
     * @param login   - login of the user
     * @param pwd     - password of the user
     * @param title   - title of the book
     * @param kind    - genre of the book
     * @param author  - author of the book
     * @param nbPages - number of pages in the book
     * @param testId  - ID of the test
     * @return 0 if the test is successful, 1 if not
     */
    private static int addItemBookOKTest(ISocialNetwork sn, String login,
                                         String pwd, String title, String kind, String author, int nbPages, String testId) {
        int nbItemBooks = sn.nbBooks(); // Number of books before attempting to add
        try {
            sn.addItemBook(login, pwd, title, kind, author, nbPages); // Try to add a book
            if (sn.nbBooks() != nbItemBooks + 1) { // Number of books didn't increase as expected
                System.out.println("Err " + testId + " : the number of books (" + nbItemBooks + ") was not incremented");
                return 1; // Return 1 for failure
            } else
                return 0; // Test passed
        } catch (Exception e) { // Unexpected exception
            System.out.println("Err " + testId + " : unexpected exception " + e);
            e.printStackTrace();
            return 1; // Return 1 for failure
        }
    }

    /**
     * Main test method for addItemBook().
     * <p>
     * This method performs various tests:
     * <ul>
     * <li>Tests if incorrect member parameters cause addItemBook() to throw NotMember exception.</li>
     * <li>Tests if incorrect book parameters cause addItemBook() to throw BadEntry exception.</li>
     * <li>Tests if adding already existing books cause addItemBook() to throw ItemBookAlreadyExists exception.</li>
     * </ul>
     * </p>
     *
     * @return a summary of the performed tests
     */
    public static TestReport test(SocialNetwork sn) {
        try {
            sn.addMember("person", "mdpasse", "gentil.");
        } catch (Exception e) {
            System.out.println("Unexpected exception while using addMember");
        }

        int nbMembers = sn.nbMembers(); // Number of members in the ISocialNetwork 1
        int nbFilms = sn.nbFilms(); // Number of films in the ISocialNetwork 0

        int nbTests = 0; // Total number of performed tests
        int nbErrors = 0; // Total number of failed tests

        System.out.println("Testing addItemBook()");

        // Test 1: NotMemberException tests
        nbTests++;
        nbErrors += addItemBookNotMemberTest(sn, null, "qsdfgh", "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", 500, "1.1", "addItemBook() allows null login to add books");
        nbErrors += addItemBookNotMemberTest(sn, "jonathan", "qsdfgh", "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", 500, "1.2", "addItemBook() allows non-existent users to add books");
        nbTests++;
        nbErrors += addItemBookNotMemberTest(sn, "person", null, "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", 500, "1.3", "addItemBook() allows users with null passwords to add books");
        nbErrors += addItemBookNotMemberTest(sn, "person", "motdepasse", "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", 500, "1.4", "addItemBook() allows users with incorrect passwords to add books");

        // Test 2: BadEntryException tests
        nbTests += 8;
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", null, "fantasy",
                "J.R.R Tolkien", 500, "2.1", "addItemBook() allows books with null titles to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "", "fantasy",
                "J.R.R Tolkien", 500, "2.2", "addItemBook() allows books with empty titles to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "      ", "fantasy",
                "J.R.R Tolkien", 500, "2.3", "addItemBook() allows books with titles made of spaces to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", null,
                "J.R.R Tolkien", 500, "2.4", "addItemBook() allows books with null kinds to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", "",
                "J.R.R Tolkien", 500, "2.5", "addItemBook() allows books with empty kinds to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", "     ",
                "J.R.R Tolkien", 500, "2.6", "addItemBook() allows books with kinds made of spaces to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", "fantasy",
                null, 500, "2.7", "addItemBook() allows books with null authors to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", "fantasy",
                "", 500, "2.8", "addItemBook() allows books with empty authors to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", "fantasy",
                "      ", 500, "2.9", "addItemBook() allows books with authors made of spaces to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", -12, "2.10", "addItemBook() allows books with number of pages less than 2 to be added");
        nbErrors += addItemBookBadEntryTest(sn, "person", "mdpasse", "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", 2500, "2.11", "addItemBook() allows books with number of pages more than 2000 to be added");

        // Test 3: ItemBookAlreadyExistsException tests
        nbTests += 3;
        nbErrors += addItemBookOKTest(sn, "person", "mdpasse", "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", 500, "noId");
        nbErrors += addItemBookOKTest(sn, "person", "mdpasse", "House Of Leaves", "psychological horror",
                "Mark Z. Danielewski", 500, "noId");
        nbErrors += addItemBookOKTest(sn, "person", "mdpasse", "Foundation", "sci-fi",
                "Isaac Asimov", 500, "noId");

        nbTests += 3;
        nbErrors += addItemBookAlreadyExistsTest(sn, "person", "mdpasse", "Lord Of The Rings", "fantasy",
                "J.R.R Tolkien", 500, "3.1", "addItemBook() allows books with existing title and author to be added");
        nbErrors += addItemBookAlreadyExistsTest(sn, "person", "mdpasse", "   Lord Of The   Rings    ", "fantasy",
                "J.R.R Tolkien", 500, "3.2", "addItemBook() allows books with existing title and author to be added with leading/surrounding blanks");
        nbErrors += addItemBookAlreadyExistsTest(sn, "person", "mdpasse", "   Lord Of The   Rings    ", "fantasy",
                "     J.R.R Tolkien      ", 500, "3.3", "addItemBook() allows books with existing title and author to be added with author leading/surrounding blanks");

        // Check if ISocialNetwork was modified
        nbTests += 2;
        if (nbFilms != sn.nbFilms()) {
            System.out.println("Error: the number of books was unexpectedly changed by addItemBook()");
            nbErrors++;
        }
        if (nbMembers != sn.nbMembers()) {
            System.out.println("Error: the number of members was unexpectedly changed by addItemBook()");
            nbErrors++;
        }

        // Display final state of the ISocialNetwork
        System.out.println("Final state of the social network: " + sn);

        // Print test summary
        try {
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("AddItemBookTest: " + tr);
            return tr;
        } catch (NotTestReportException e) {
            System.out.println("Unexpected error in AddMemberTest test code - Can't return valuable test results");
            return null;
        }
    }

    /**
     * Launches test().
     *
     * @param args not used
     */
    public static void main(String[] args) {
        SocialNetwork sn = new SocialNetwork();
        test(sn);
        SocialNetworkPremium snp = new SocialNetworkPremium();
        test(snp);
    }
}