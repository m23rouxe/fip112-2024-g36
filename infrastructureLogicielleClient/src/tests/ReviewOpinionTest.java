package tests;

import elements.ItemFilm;
import exceptions.*;
import opinion.ISocialNetworkPremium;
import opinion.SocialNetwork;
import opinion.SocialNetworkPremium;

public class ReviewOpinionTest {
    private static int reviewOpinionBadEntryTest(SocialNetworkPremium sn, String login, String pwd, String title, float opinion, String reviewerLogin, String testId, String errorMessage) {
        Float mean = sn.getItem(title, ItemFilm.class).getMeanReviews();
        try {
            sn.reviewOpinion(login,pwd,title,reviewerLogin,opinion); // Try to a review on someone's opinion
            System.out.printf("Err %s : %s",testId,errorMessage); // Error message
            return 1; // Return 1 for failure
        }
        catch (BadEntryException e){ // Expected NotMemberException was thrown
            if (!sn.getItem(title, ItemFilm.class).getMeanReviews().equals(mean)) { //Number of reviews changed unexpectedly
                System.out.printf("Err %s : NotMember was thrown but the mean of reviews on the item was changed",testId);
                return 1;// Return 1 for failure
            }
            else {
                return 0; //Test passed
            }
        }
        catch (Exception e) { //Unexpected exception
            System.out.printf("Err %s : unexpected exception. %s" ,testId,e);
            e.printStackTrace();
            return 1;
        }
    }

    private static int reviewOpinionNotMemberTest(SocialNetworkPremium sn, String login, String pwd, String title, float opinion, String reviewerLogin, String testId, String errorMessage) {
        Float mean = sn.getItem(title, ItemFilm.class).getMeanReviews();
        try {
            sn.reviewOpinion(login,pwd,title,reviewerLogin,opinion); // Try to a review on someone's opinion
            System.out.printf("Err %s : %s",testId,errorMessage); // Error message
            return 1; // Return 1 for failure
        }
        catch (NotMemberException e){ // Expected NotMemberException was thrown
            if (!sn.getItem(title, ItemFilm.class).getMeanReviews().equals(mean)) { //Number of reviews changed unexpectedly
                System.out.printf("Err %s : NotMember was thrown but the mean of reviews on the item was changed",testId);
                return 1;// Return 1 for failure
            }
            else {
                return 0; //Test passed
            }
        }
        catch (Exception e) { //Unexpected exception
            System.out.printf("Err %s : unexpected exception. %s" ,testId,e);
            e.printStackTrace();
            return 1;
        }
    }

    private static int reviewOpinionNotItemTest(SocialNetworkPremium sn, String login, String pwd, String title, float opinion, String reviewerLogin, String testId, String errorMessage) {
        Float karma = sn.getMember(reviewerLogin).getKarma();
        try {
            sn.reviewOpinion(login,pwd,title,reviewerLogin,opinion); // Try to a review on someone's opinion
            System.out.printf("Err %s : %s",testId,errorMessage); // Error message
            return 1; // Return 1 for failure
        }
        catch (NotItemException e){ // Expected NotMemberException was thrown
            if (!karma.equals(sn.getMember(reviewerLogin).getKarma())) { //Number of reviews changed unexpectedly
                System.out.printf("Err %s : NotMember was thrown but the mean of reviews on the item was changed",testId);
                return 1;// Return 1 for failure
            }
            else {
                return 0; //Test passed
            }
        }
        catch (Exception e) { //Unexpected exception
            System.out.printf("Err %s : unexpected exception. %s" ,testId,e);
            e.printStackTrace();
            return 1;
        }
    }


    static public TestReport test() throws Exception {

        int nbTests = 0; // Total number of performed tests
        int nbErrors = 0; // Total number of failed tests
        SocialNetworkPremium sn = new SocialNetworkPremium();

        try {
            sn.addMember("abcd", "abcd", "abcd");
            sn.addMember("efgh", "efgh", "efgh");
            sn.addItemFilm("abcd", "abcd", "film","fantasy", "someone", "somebody", 120);
            sn.reviewItemFilm("abcd","abcd","film",4.0f, "pas mal");
        }catch (Exception e){
            System.out.println("unexpected exception during test startup");
        }

        nbTests += 6;
        nbErrors += reviewOpinionNotMemberTest(sn, null, "efgh", "film", 3.0f, "abcd", "17.1", "reviewOpinion allows Members that are not instanced to review opinions");
        nbErrors += reviewOpinionNotMemberTest(sn, "", "efgh", "film", 3.0f, "abcd", "17.2", "reviewOpinion allows Members that are not registered to review opinions");
        nbErrors += reviewOpinionNotMemberTest(sn, "efgh", null, "film", 3.0f, "abcd", "17.3", "reviewOpinion allows Members with not instanced passwords to review opinions");
        nbErrors += reviewOpinionNotMemberTest(sn, "efgh", "", "film", 3.0f, "abcd", "17.4", "reviewOpinion allows Members with wrong passwords to review opinions");
        nbErrors += reviewOpinionNotMemberTest(sn, "efgh", "efgh", "film", 3.0f, null, "17.5", "reviewOpinion allows to review opinions of people that are not instanced");
        nbErrors += reviewOpinionNotMemberTest(sn, "efgh", "efgh", "film", 3.0f, "", "17.6", "reviewOpinion allows to review opinions of people that did not review the specified item");

        nbTests += 2;
        nbErrors += reviewOpinionNotItemTest(sn, "efgh", "efgh", null, 3.0f, "abcd", "18.1", "reviewOpinion allows to review opinions on not instanced products");
        nbErrors += reviewOpinionNotItemTest(sn, "efgh", "efgh", "", 3.0f, "abcd", "18.2", "reviewOpinion allows to review opinions on unregistered products");

        nbTests += 2;
        nbErrors += reviewOpinionBadEntryTest(sn, "efgh","efgh", "film", 6, "abcd", "19.1", "reviewOpinion allows to review opinions with marks superior to 5");
        nbErrors += reviewOpinionBadEntryTest(sn, "efgh","efgh", "film", -1, "abcd", "19.2", "reviewOpinion allows to review opinions with marks superior to 5");

        // Print test summary
        try {
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.printf("ReviewOpinionTest: %s\n", tr);
            return tr;
        } catch (NotTestReportException e) {
            System.out.println("Unexpected error in AddMemberTest test code - Can't return valuable test results");
            return null;
        }
    }

    public static void main(String[] args) throws Exception{
        test();
    }
}

