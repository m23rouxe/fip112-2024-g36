# ToutAvis, collaborative opinion based social network

Generated JavaDoc for the project : [javadoc for fip112-2024-g36](https://fip112-2024-g36-m23rouxe-49ff420985e26fb39202533ed88a99bc7af67e.gitlab-pages.imt-atlantique.fr/)

Release page : https://gitlab.imt-atlantique.fr/m23rouxe/fip112-2024-g36/-/releases

## Project features
- ISocialNetwork interface providing signatures to be implemented
- User authentication
- Generalized Item types based products
- Ability to review products

## Technologies
- Java 21 ([open-jdk](https://openjdk.org/projects/jdk/21/))
- JavaDoc
- Implementation ideas :
    - HashMaps for review unicity on each Item
    - Generalization for scalability

## Releases
See the [Release page](https://gitlab.imt-atlantique.fr/m23rouxe/fip112-2024-g36/-/releases)

- 1.1 : [Member management](https://gitlab.imt-atlantique.fr/m23rouxe/fip112-2024-g36/-/releases/v0.1)
- 1.2 : [ItemBook management](https://gitlab.imt-atlantique.fr/m23rouxe/fip112-2024-g36/-/releases/v0.2.1)
- 1.3 : [Item Generalization and Review management](https://gitlab.imt-atlantique.fr/m23rouxe/fip112-2024-g36/-/releases/v0.3)
    - report : [1.3 release report](https://gitlab.imt-atlantique.fr/m23rouxe/fip112-2024-g36/-/blob/main/reports/INF112_LOT1_3.pdf)
